// cmdtest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../public/shade/base.h"
#include <algorithm>
#include "../public/shade/render/native.h"


int _tmain(int argc, _TCHAR* argv[])
{
	shade::Handle<shade::CNative> h = shade::CreateNativeDX9( (HINSTANCE)::GetModuleHandle(NULL), "Hello World!" );
	h->Show();
	h->MessageLoop();

	return 0;
}

