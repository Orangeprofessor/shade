
#include "time.h"
#include <Windows.h>

namespace shade
{

void SHADECC QueryTime( time_t& time, time_t& freq )
{
	::QueryPerformanceCounter( (LARGE_INTEGER*)&time );
	::QueryPerformanceFrequency( (LARGE_INTEGER*)&freq );
	static time_t base = 0;
	if ( !base )
	{
		base = time;
	}
	time -= base;
}
NOINLINE mstime_t SHADECC MsecTime()
{
	time_t time, freq;
	QueryTime( time, freq );
	// FIXME! This produces lots of library calls...
	return static_cast<mstime_t>( time / ( freq/1000 ) );
}
NOINLINE fltime_t SHADECC FloatTime()
{
	time_t time, freq;
	QueryTime( time, freq );
	return static_cast<fltime_t>( static_cast<double>( time ) / static_cast<double>( freq ) );
}

}
