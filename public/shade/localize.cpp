
#include "localize.h"
#include "config/keyvalues.h"
#include "config/parser.h"

namespace shade
{


void ILocalize::Parse( KvNode* kv )
{
	// Compare against the current language?
	//if ( !strcmp( kv->attr, lang ) ) ...;

	idtype_t id = 0;

	switch ( id )
	{
	// lang: clear;
	case 0x0a8ba07c:
		RemoveAll();
		break;

	//// lang: load;
	//case 0x7c6f2463:
	//	{
	//		const char* lang = Language();
	//		// Fetch the language itself...
	//		// WARNING! Relies on "#LangFile" to be available!!!
	//		char file[64];
	//		char base[8];
	//		//_snprintf( file, sizeof(file), "lang/%s.txt", lang );
	//		if ( Format( "#LangFile", file, sizeof(file), (lang && lang[0])?lang:(((__int64*)base)[0]=*(const __int64*)"base\0\0\0\0",base) )>0 )
	//			ui->Resources()->LoadFromFile( file, &kv->context );
	//		//else ThrowResourceError( kv, "Localize::Parse()", "Load lang/vgui.txt first!" );
	//	}
	//	break;

	//// lang: reload;
	//case 0x7ea50774:
	//	ReloadTranslations( ui->Resources(), &kv->context );
	//	break;

	// Language definitions
	case 0x00001505:
		for ( KvNode* it = kv->child; it; it = it->flink )
		{
			if ( !AddString( it->ident, it->value ) )
				Parser.Throw( it, "Localize::Parse()", "Collision!" );
		}
		break;

	// Error
	default:
		Parser.Throw( kv, "Localize::Parse()", "Unknown '%s'!", kv->value );
	}
}
int ILocalize::ConvertToUtf8( char* out, int size, const wchar_t* in, int len ) const
{
	// TODO!
	return 0;
}
int ILocalize::ConvertToUtf16( wchar_t* out, int size, const char* in, int len ) const
{
	if ( len<0 )
		len = strlen(in);
	const char* end = in+len;

	size = size/sizeof(wchar_t);
	int written = 0;
	while ( in<end && written<size )
	{
		wchar_t c = DecodeUtf8( in );
		*out++ = c;
	}
	*out = 0;
	return written;
}



NOINLINE unsigned int SHADECC DecodeUtf8( const char*& ptr, unsigned int res )
{
	// FIXME! Must be secure to be utf-8 compliant!
	// http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-test.txt

	const unsigned char* it = (const unsigned char*)ptr;
	unsigned int c = it[0];

	// Most common test first
	if ( c<0x80 )
	{
		res = c;
		it += 1;
	}
	// These could be optimized by grouping the tests together but I doubt that makes a difference...
	else if ( c>=0xFC )
	{
		res  = (it[0]&0x01) << 30;
		res |= (it[1]&0x3F) << 24;
		res |= (it[2]&0x3F) << 18;
		res |= (it[3]&0x3F) << 12;
		res |= (it[4]&0x3F) << 6;
		res |= (it[5]&0x3F);
		it += 6;
	}
	else if ( c>=0xF8 )
	{
		res  = (it[0]&0x03) << 24;
		res |= (it[1]&0x3F) << 18;
		res |= (it[2]&0x3F) << 12;
		res |= (it[3]&0x3F) << 6;
		res |= (it[4]&0x3F);
		it += 5;
	}
	else if ( c>=0xF0 )
	{
		res  = (it[0]&0x07) << 18;
		res |= (it[1]&0x3F) << 12;
		res |= (it[2]&0x3F) << 6;
		res |= (it[3]&0x3F);
		it += 4;
	}
	else if ( c>=0xE0 )
	{
		res  = (it[0]&0x0F) << 12;
		res |= (it[1]&0x3F) << 6;
		res |= (it[2]&0x3F);
		it += 3;
	}
	else if ( c>=0xC0 )
	{
		res  = (it[0]&0x1F) << 6;
		res |= (it[1]&0x3F);
		it += 2;
 	}
	else /*if ( c>=0x80 )*/
	{
//invalid:
		// Error: invalid
		it += 1;
	}

	ptr = (const char*)it;
	return res;
}

}
