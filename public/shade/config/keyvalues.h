#ifndef HGUARD_SHADE_CONFIG_KEYVALUES
#define HGUARD_SHADE_CONFIG_KEYVALUES
#pragma once

#include "../base.h"

namespace shade
{

// Context for files
struct KvContext
{
	char_t* file;
	unsigned int line;
	unsigned int offset;
	KvContext* parent;
};

// Each node represented
struct KvNode
{
	// Values
	char_t* ident;
	char_t* value;
	char_t** values;
	char_t* attr;
	// Links
	KvNode* child;
	KvNode* flink;
	KvNode* blink;
	// Context
	KvContext ctx;
};

}

#endif // !HGUARD_SHADE_CONFIG_KEYVALUES
