#ifndef HGUARD_SHADE_CONFIG_PARSER
#define HGUARD_SHADE_CONFIG_PARSER
#pragma once

#include "keyvalues.h"

namespace shade
{

class KvLexer;
struct enum_t;


class CParser
{
public:
	// Managing
	virtual void ThrowEx( const KvContext* ctx, const char* sub, const char* fmt, va_list va = nullptr ) const;
			void Throw( KvNode* kv, const char* sub, const char* fmt, ... ) const;

	// Find nodes with error checking
	virtual KvNode* Find( KvNode* kv, idtype_t name ) const;
	template< unsigned int L > KvNode* Find( KvNode* kv, const char (&name)[L] ) const;
			KvNode* Look( KvNode* kv, idtype_t name ) const;

	// Parse & process a resource
	typedef bool (SHADECC* HandlerFn)( void* data, KvNode* kv );
	typedef bool (SHADECC* ErrorFn)( const std::exception& err, const KvContext& ctx );

	// Create a resource lexer class, make sure you delete it when done!
	virtual KvLexer* Lexer() const;
	virtual void Process( void* data, const char* text, HandlerFn pfn, const KvContext& ctx, ErrorFn err = nullptr ) const;
	
	// Primitive types
	virtual int Integer( KvNode* kv, const int bounds[] = nullptr ) const; // Array of 2 ints, [first,second]
	virtual void LargeInt( KvNode* kv, __int64& out ) const;
	virtual unsigned int IntArray( KvNode* kv, int* out, int count ) const;
	template< unsigned int N > inline unsigned int IntArray( KvNode* kv, int (&out)[N] ) const { return IntArray( kv, out, N ); }

	virtual double Number( KvNode* kv ) const;
	inline float Float( KvNode* kv ) const { return static_cast<float>( Number( kv ) ); }
	virtual unsigned int Vector( KvNode* kv, float* out, int count ) const;
	template< unsigned int N > inline unsigned int Vector( KvNode* kv, float (&out)[N] ) const { return Vector( kv, out, N ); }
	
	// Enumerations
	virtual unsigned int Enum( KvNode* kv, const enum_t* e ) const;
	virtual bool Bool( KvNode* kv ) const;

	virtual color_t Color( KvNode* kv ) const;
	
protected:
	// Parser errors
	// Contains line numbers for tracing the error
	class Error : public std::exception
	{
		friend class CParser;
		Error( const KvContext* ctx, const char* str );
	public:
		virtual const char* what() const override;
		virtual bool Handle( ErrorFn pfn, const KvContext& ctx ) const;
	private:
		mutable bool supress;
		mutable string_t str;
	};
} Parser;


void CParser::Throw( KvNode* kv, const char* sub, const char* fmt, ... ) const
{
	va_list va;
	va_start( va, fmt );
	ThrowEx( &kv->ctx, sub, fmt, va );
}

}

#endif // !HGUARD_SHADE_CONFIG_PARSER
