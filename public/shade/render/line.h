#ifndef HGUARD_SHADE_RENDER_LINE
#define HGUARD_SHADE_RENDER_LINE
#pragma once

#include "shared.h"

namespace shade
{

INTERFACE IRenderLine
{
public:
	// Set pen color
	virtual void SetPenColor( color_t clr ) = 0;
	// Set pen properties
	virtual void SetPenStyle( unit width = 0, int pattern = 0, int caps = 0 ) = 0;
	// Move the cursor to coordinates
			void MoveTo( unit x, unit y ) = 0;
	// Draw a line from current cursor position to coordinates
	virtual void LineTo( unit x, unit y ) = 0;
	// Draw a line
			void DrawLine( unit x1, unit y1, unit x2, unit y2 );

	// Draw a rectangle
	virtual void DrawRect( const rect& rc ) = 0;
			void DrawRect( unit left, unit top, unit right, unit bottom );
			void DrawLines( const point* pts, unsigned n, bool loop );
			template< unsigned N >
			void DrawLines( const point (&pts)[N], bool loop );

protected:
	unit lineX;
	unit lineY;
};




inline void IRenderLine::DrawLine( unit x1, unit y1, unit x2, unit y2 )
{
	MoveTo( x1, y1 );
	LineTo( x2, y2 );
}
inline void IRenderLine::DrawRect( unit left, unit top, unit right, unit bottom )
{
	MoveTo( left, top );
	LineTo( right, top );
	LineTo( right, bottom );
	LineTo( left, bottom );
	LineTo( left, top );
}
inline void IRenderLine::DrawLines( const point* pts, unsigned int n, bool loop )
{
	MoveTo( pts[0].x, pts[0].y );
	for ( const point* end = pts++ + n; pts<end; ++pts )
	{
		LineTo( pts[0].x, pts[0].y );
	}
	if ( loop )
	{
		LineTo( pts[0].x, pts[1].y );
	}
}
template< unsigned N >
inline void IRenderLine::DrawLines( const point (&pts)[N], bool loop )
{
	DrawLines( pts, N, loop );
}

}

#endif // !HGUARD_SHADE_RENDER_LINE
