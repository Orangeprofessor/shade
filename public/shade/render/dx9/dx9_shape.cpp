
#include "dx9.h"
#include "../bezier.h"

namespace shade
{
namespace dx9
{

void CRender::SetPenColor( color_t clr )
{
	lineColor = clr;
	lineSegments = 64;
}
void CRender::SetPenStyle( unit width, int pattern, int caps )
{
}
void CRender::MoveTo( unit x, unit y )
{
	lineX = x;
	lineY = y;
	lineColor2 = lineColor;
}
void CRender::LineTo( unit x, unit y )
{
	// Vertices: 2
	// Primitives: 1
	// Indices: 2

	cache.Begin( currentContext, cache.BTYPE_LINES, 2, 1, TECH_DIFFUSE );

	cache.AddIndex( 0 );
	cache.AddIndex( 1 );

	cache.AddVert( float(lineX), float(lineY), lineColor2.raw() );
	cache.AddVert( float(x), float(y), lineColor.raw() );
	lineColor2 = lineColor;

	cache.End();

	lineX = x;
	lineY = y;
}
void CRender::DrawRect( const rect_t& rc )
{
	// Vertices: 4
	// Primitives: 4
	// Indices: 8

	cache.Begin( currentContext, cache.BTYPE_LINES, 4, 4, TECH_DIFFUSE );

	cache.AddIndex( 0 );
	cache.AddIndex( 1 );
	cache.AddIndex( 1 );
	cache.AddIndex( 2 );
	cache.AddIndex( 2 );
	cache.AddIndex( 3 );
	cache.AddIndex( 3 );
	cache.AddIndex( 0 );

	DWORD color = lineColor.raw();
	unit x = rc.left;
	unit y = rc.top;
	unit r = x+rc.width-1;
	unit b = y+rc.height-1;

	cache.AddVert( float(x), float(y), color );
	cache.AddVert( float(r), float(y), color );
	cache.AddVert( float(r), float(b), color );
	cache.AddVert( float(x), float(b), color );

	cache.End();
}
void CRender::DrawRoundRect( const rect_t& rc, unit sx, unit sy )
{
	// Fixup parameters

	if ( sx+sx>rc.width )
	{
		sx = rc.width/2;
	}
	if ( sy+sy>rc.height )
	{
		sy = rc.height/2;
	}
	if ( sx<=0 || sy<=0 )
	{
		DrawRect( rc );
	}
	else
	{
	// Just draw the straight edges first

	unit x = rc.left;
	unit y = rc.top;
	unit r = x+rc.width-1;
	unit b = y+rc.height-1;

	DrawLine( x+sx, y, r-sx, y );
	DrawLine( r, y+sy, r, b-sy );
	DrawLine( r-sx, b, x+sx, b );
	DrawLine( x, b-sy, x, y+sy );

	// Draw the rounded corners as quad beziers
	// NOTE! Can be done with cubic beziers
	// https://en.wikipedia.org/wiki/B%C3%A9zier_spline#Approximating_circular_arcs

	lineSegments = 4;

	point_t pts[3];
	pts[0] = point_t( r-sx, y );
	pts[1] = point_t( r, y );
	pts[2] = point_t( r, y+sy );
	DrawBezierQuad( pts );
	pts[0] = point_t( r, b-sy );
	pts[1] = point_t( r, b );
	pts[2] = point_t( r-sx, b );
	DrawBezierQuad( pts );
	pts[0] = point_t( x+sx, b );
	pts[1] = point_t( x, b );
	pts[2] = point_t( x, b-sy );
	DrawBezierQuad( pts );
	pts[0] = point_t( x, y+sy );
	pts[1] = point_t( x, y );
	pts[2] = point_t( x+sx, y );
	DrawBezierQuad( pts );
	}
}
void CRender::DrawEllipse( const rect_t& rc )
{
	// Basic algorithm kindly taken from
	// http://slabode.exofire.net/circle_draw.shtml

	float cx, cy, rx, ry;
	unsigned int segs;
	DWORD color;

	rx = static_cast<float>( rc.width-1 ) / 2.0f;
	cx = static_cast<float>( rc.left ) + rx;
	ry = static_cast<float>( rc.height-1 ) / 2.0f;
	cy = static_cast<float>( rc.top ) + ry;
	segs = (static_cast<unsigned int>( 5.0f * sqrtf( rx+ry ) )&~3)+4;
	color = lineColor.raw();

	// Vertices: segs
	// Primitives: segs
	// Indices: segs*2

	cache.Begin( currentContext, CCache::BTYPE_LINES, segs, segs, TECH_DIFFUSE );

	cache.AddIndex( 0 );
	for ( unsigned int i = 1; i<segs; i++ )
	{
		cache.AddIndex2( i, i );
	}
	cache.AddIndex( 0 );

	// Precompute trigs
	float theta = 2.0f*3.1415926f / static_cast<float>(segs);
	float s = sinf( theta );
	float c = cosf( theta );
	float t;

	float x = 1.0f;
	float y = 0.0f; 

	for( unsigned int i = 0; i<segs; i++ ) 
	{
		cache.AddVert( x*rx + cx, y*ry + cy, color );

		// Apply the rotation matrix
		t = x;
		x = c * x - s * y;
		y = s * t + c * y;
	}

	cache.End();
}
void CRender::DrawBezierQuad( const point_t pts[3] )
{
	cache.Begin( currentContext, cache.BTYPE_LINES, lineSegments+1, lineSegments, TECH_DIFFUSE );
	for ( int i = 0; i<lineSegments; ++i ) cache.AddIndex2( i, i+1 );
	cache.AddVert( float(pts[0].x), float(pts[0].y), lineColor.raw() );

	float
		x1 = static_cast<float>(pts[0].x),
		y1 = static_cast<float>(pts[0].y),
		x2 = static_cast<float>(pts[1].x),
		y2 = static_cast<float>(pts[1].y),
		x3 = static_cast<float>(pts[2].x),
		y3 = static_cast<float>(pts[2].y);

	for ( int i = 1; i<=lineSegments; ++i )
	{
		float t = float(i) / float(lineSegments);
		float s = 1.0f - t;

		float x = s*s*x1 + 2*s*t*x2 + t*t*x3;
		float y = s*s*y1 + 2*s*t*y2 + t*t*y3;

		cache.AddVert( x, y, lineColor.raw() );
	}

	cache.End();
}
void CRender::DrawBezierCubed( const point_t pts[4] )
{
	cache.Begin( currentContext, cache.BTYPE_LINES, lineSegments+1, lineSegments, TECH_DIFFUSE );
	for ( int i = 0; i<lineSegments; ++i ) cache.AddIndex2( i, i+1 );
	cache.AddVert( float(pts[0].x), float(pts[0].y), lineColor.raw() );

#ifdef SHADE_UNIT_INT
	__m128 apts[2];
	apts[0] = _mm_cvtepi32_ps( _mm_loadu_si128( (const __m128i*)&pts[0] ) );
	apts[1] = _mm_cvtepi32_ps( _mm_loadu_si128( (const __m128i*)&pts[2] ) );
#endif // SHADE_UNIT_INT

	float segs = 1.0f / static_cast<float>(lineSegments);

	for ( int i = 1; i<=lineSegments; ++i )
	{
		Vertex& vert = cache.AddVert();
		vert.color = lineColor.raw();
		SSE_CubicBezier( apts, static_cast<float>(i)*segs, &vert.x );
	}

	cache.End();
}


void CRender::FillRect( const rect_t& rc )
{
	// Vertices: 4
	// Primitives: 2
	// Indices: 6

	cache.Begin( currentContext, CCache::BTYPE_TRIS, 4, 2, TECH_DIFFUSE );
	cache.AddIndicesQuad();

	unit x = rc.left;
	unit y = rc.top;
	unit b = y+rc.height;

	DWORD color = brushColor.raw();
	cache.AddVert( float(x), float(b), color );
	cache.AddVert( float(x), float(y), color );
	x += rc.width;
	cache.AddVert( float(x), float(y), color );
	cache.AddVert( float(x), float(b), color );

	cache.End();
}
void CRender::FillConvex( const point_t* pts, const float* uvs, int count )
{
}
void CRender::FillPolygon( const Polygon& poly )
{
	// TODO! :)
}
void CRender::FillBezierQuad( const point_t pts[3], const float uvs[6] )
{
}


}
}
