#ifndef HGUARD_SHADE_RENDER_DX9_CACHEDX9
#define HGUARD_SHADE_RENDER_DX9_CACHEDX9
#pragma once

#include "shared.h"

namespace shade
{
namespace dx9
{

class CShader;
struct texture_t;

//
// Vertex batching
//
// In order to optimize things primitives are batched:
// This means that if you draw 2 primitives, their properties are cached (vertices, colors, textures)
// so that their data can be sent in 1 copy to the gpu's vertex buffer and then rendered in as few as possible drawing calls.
//

class CCache
{
public:
	CCache();
	~CCache();

	// Primitive types
	enum btype_t
	{
		BTYPE_EMPTY = 0,
		BTYPE_LINES = 2, // D3DPT_LINELIST
		BTYPE_TRIS = 4, // D3DPT_TRIANGLELIST
	};
	// Batch info
	struct Batch
	{
		unsigned char type;
		unsigned char params;
		unsigned short numvert;
		unsigned short numprim;
		unsigned short numidx;
		texture_t* tex;
		idtype_t tech;
		context_t ctx;
	};
	// Indicies
	typedef unsigned short Index;
	// Buffer limits
	enum
	{
		MAXVERTS = 512,
		MAXBATCHES = 32,
		MAXINDICES = 1024,
		MAXSIZE = sizeof(Vertex)*MAXVERTS + sizeof(Batch)*MAXBATCHES + sizeof(Index)*MAXINDICES,
	};

	// Init stuff
	void Init();
	void Release();

	// Render everything
	void Render();
	void Clear();
	void Blend( blend_t blend );

	// Read performance and resets the stats
	//void Stats( perf_t::cache_t& out );

	// Start feeding verts for a single primitive
	bool Begin( const context_t& ctx, btype_t type, unsigned int verts, unsigned int prims, idtype_t tech, texture_t* tex = nullptr );
	void End();
	void NewBatch();

	// Add an index
	inline void AddIndex( unsigned short vert )
	{
		indices[iIndex++] = iVert + vert;
	}
	// Add 2 indices
	inline void AddIndex2( unsigned short v1, unsigned short v2 )
	{
		indices[iIndex+0] = iVert+v1;
		indices[iIndex+1] = iVert+v2;
		iIndex += 2;
	}
	// Adds indices for a quad, vertex order:
	// 2---3
	// | / |
	// 1---4
	void AddIndicesQuad();
	// Add a single vertex
	inline Vertex& AddVert()
	{
		Vertex& vert = verts[iVert++];
		return vert;
	}
	// Add a single color vertex
	inline Vertex& AddVert( float x, float y, unsigned int color )
	{
		Vertex& vert = verts[iVert++];
		vert.x = x;
		vert.y = y;
		vert.color = color;
		return vert;
	}
	// Add a textured vertex
	inline Vertex& AddVert( float x, float y, float u, float v, unsigned int color = -1 )
	{
		Vertex& vert = verts[iVert++];
		vert.x = x;
		vert.y = y;
		vert.color = color;
		vert.u = u;
		vert.v = v;
		return vert;
	}
	// Add vertex with maybe u/v
	inline Vertex& AddVert( float x, float y, float uv[2], unsigned int color )
	{
		Vertex& vert = verts[iVert++];
		vert.x = x;
		vert.y = y;
		vert.color = color;
		if ( uv )
		{
			vert.u = uv[0];
			vert.v = uv[1];
		}
	}

private:
	IDirect3DVertexBuffer9* vbuf;
	IDirect3DIndexBuffer9* ibuf;

	unsigned int iBatch, iVert, iIndex;

	Batch* batches;
	Vertex* verts;
	Index* indices;

	color_t colormod;

#ifdef SHADE_DEBUG
	// These are for debugging
	unsigned int iVertEnd, iIndexEnd;
	bool debug;
#endif // SHADE_DEBUG

	perf_t::cache_t stats;
};

}
}

#endif // !HGUARD_SHADE_RENDER_DX9_CACHEDX9
