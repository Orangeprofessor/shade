
#include "shared.h"

namespace shade
{
namespace dx9
{

void DxError::format( string_t& str ) const
{
#ifdef SHADE_DEBUG
	str = tools::va_printf<1024>( TEXT("HRESULT 0x%08X - %s\nDescription: %s"),
		hr, ::DXGetErrorString(hr), ::DXGetErrorDescription(hr) );
#else
	str = tools::va_printf<256>( TEXT("HRESULT 0x%08X"), hr );
#endif // SHADE_DEBUG
}
DxCompileError::DxCompileError( HRESULT hr, ID3DXBuffer* err, const char_t* file ) :
	DxError(hr), filename(file)
{
	if ( err )
	{
		// Get description, missing files have no error...
		desc = (const char*)err->GetBufferPointer();
		// Release the buffer here, since this will otherwise get leaked!
		err->Release();
	}
}
void DxCompileError::format( string_t& str ) const
{
	DxError::format( str );
	str.append(
		tools::va_printf<1024>( TEXT("\nCompile error in '%s':\n%s"), filename.c_str(), desc.c_str() )
	);
}


void perf_t::begin()
{
	// Smooth the fps counter a bit
	const float smooth = 0.9f;
	float raw = static_cast<float>( 1.0 / timer.Delta() );
	fps = raw + (fps-raw)*smooth;
	//fps = fps*0.9f + 0.1f*raw;
	timer.Sync();
}

}
}
