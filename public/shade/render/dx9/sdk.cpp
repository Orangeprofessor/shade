
#include "sdk.h"

namespace shade
{
namespace dx9
{

d3dx9_t& d3dx9_t::get()
{
	static d3dx9_t imps;

	HMODULE hmD3DX9 = ::LoadLibrary( TEXT("d3dx9_43.dll") );
	imps.CreateEffect.pfn = ::GetProcAddress( hmD3DX9, "CreateEffect" );
	imps.DisassembleEffect.pfn = ::GetProcAddress( hmD3DX9, "DisassembleEffect" );
	return imps;
}

}
}
