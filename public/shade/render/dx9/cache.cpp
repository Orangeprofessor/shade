
#include "cache.h"

namespace shade
{
namespace dx9
{
	
#define THIS GETOUTER(CRender,this,cache)
#define dev (THIS->dev)

enum {
	D3DFVF_VERTEX = (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1),
};

CCache::CCache()
{
	Init();

	// Alloc memory for the RAM buffers

	void* mem = ::malloc( MAXSIZE );
	batches = (Batch*) mem;
	verts = (Vertex*) (batches+MAXBATCHES);
	indices = (Index*) (verts+MAXVERTS);

	// Init first batch
	Clear();

#ifdef SHADE_DEBUG
	iVertEnd = 0;
	iIndexEnd = 0;
	debug = true;
#endif // SHADE_DEBUG
}
CCache::~CCache()
{
	::free( batches );
}
void CCache::Init()
{
	// Create the D3D buffers

	HRESULT hra = dev->CreateVertexBuffer( MAXVERTS*sizeof(Vertex), D3DUSAGE_DONOTCLIP|D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY, D3DFVF_VERTEX, D3DPOOL_DEFAULT, &vbuf, NULL );
	HRESULT hrb = dev->CreateIndexBuffer( MAXINDICES*sizeof(Index), D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &ibuf, NULL );

	if ( HRESULT hr = FAILED(hra) ? hra : FAILED(hrb) ? hrb : 0 )
	{
		throw DxError( hr );
	}
}
void CCache::Release()
{
	if ( vbuf ) { vbuf->Release(); vbuf = NULL; }
	if ( ibuf ) { ibuf->Release(); ibuf = NULL; }
}
void CCache::Render()
{
	if ( !iVert )
		return;

	// perf_t

	stats.verts += iVert;
	stats.indices += iIndex;
	stats.flushes += 1;
	
	// Copy to video memory
	// Flags to indicate these buffers are thrown away after use

	HRESULT hr;

	void* vmem;
	hr = vbuf->Lock( 0, iVert*sizeof(Vertex), &vmem, D3DLOCK_DISCARD|D3DLOCK_NOSYSLOCK );
	assert( SUCCEEDED(hr) );
	//__movsd( (unsigned long*)vmem, (const unsigned long*)verts, iVert*sizeof(Vertex)/sizeof(unsigned long) );
	memcpy( vmem, verts, iVert*sizeof(Vertex) );
	hr = vbuf->Unlock();
	assert( SUCCEEDED(hr) );
	hr = ibuf->Lock( 0, iIndex*sizeof(Index), &vmem, D3DLOCK_DISCARD|D3DLOCK_NOSYSLOCK );
	assert( SUCCEEDED(hr) );
	memcpy( vmem, indices, iIndex*sizeof(Index) );
	hr = ibuf->Unlock();
	assert( SUCCEEDED(hr) );

	// Setup the device

	hr = dev->SetStreamSource( 0, vbuf, 0, sizeof(Vertex) );
	assert( SUCCEEDED(hr) );
	hr = dev->SetIndices( ibuf );
	assert( SUCCEEDED(hr) );

	// Render each batch
	// Note that it needs <= since iBatch points to the *current* batch
	//  which may or may not be specialized to lines or tris yet

	unsigned int svert = 0;
	unsigned int sindex = 0;
	typedef const Batch* iterator;
	for ( iterator b = batches, end = b+iBatch; b<=end; ++b )
	{
//#ifdef SHADE_DEBUG
		if ( !( b->type==BTYPE_LINES || b->type==BTYPE_TRIS || b->type==BTYPE_EMPTY ) )
		{
			// BUGBUG!
			// This may crash, looks like an uninitialized batch or something!!!
			// Find out in which case an uninitialized batch ever gets here!
			//__debugbreak();
			// 18/8 STILL NOT FIXED! CRASHES IN TF2!!!
			continue;
		}
//#endif // SHADE_DEBUG

		if ( b->type && b->numprim )
		{
			//stats.batches++;
			
			// Set blend mode
			Blend( b->ctx.blend ); 
			
			// Render the primitives
			CShader& shader = THIS->shader;
			shader.Setup( b );
			for ( unsigned int pass = 0; shader.Pass( pass ); ++pass )
			{
				hr = dev->DrawIndexedPrimitive( static_cast<D3DPRIMITIVETYPE>(b->type), 0, svert, b->numvert, sindex, b->numprim );
				assert( SUCCEEDED(hr) );
			}
#ifdef SHADE_DEBUG
			//if ( debug && b->type==BTYPE_TRIS )
			//{
			//	// Draw textures as wireframe
			//  // Note: huge fps hit
			//	dev->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );
			//	dev->DrawIndexedPrimitive( static_cast<D3DPRIMITIVETYPE>(BTYPE_TRIS), 0, svert, b->numvert, sindex, b->numprim );
			//	dev->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );
			//}
#endif // SHADE_DEBUG
			shader.Finish();
		}

		svert += b->numvert;
		sindex += b->numidx;
	}
	// Reset
	Clear();
}
void CCache::Clear()
{
	iBatch = 0;
	iVert = 0;
	iIndex = 0;
#ifdef SHADE_DEBUG
	// Patternize the free memory
	memset( batches, 0xCD, MAXSIZE );
#endif // SHADE_DEBUG
	batches[0].type = BTYPE_EMPTY;
}
void CCache::Blend( blend_t blend )
{
	// Source for the formulas:
	// http://dunnbypaul.net/blends/

	struct rs_t { DWORD src; DWORD dest; DWORD op; };
	static const rs_t list[] =
	{
		// BLEND_SOLID: final = src
		{ D3DBLEND_SRCCOLOR, D3DBLEND_ZERO, D3DBLENDOP_ADD },
		// BLEND_ALPHA: final = (src*srca) + (dest*(1-srca))
		{ D3DBLEND_SRCALPHA, D3DBLEND_INVSRCALPHA, D3DBLENDOP_ADD },

		// BLEND_LIGHTEN: final = max( src, dest )
		{ D3DBLEND_SRCALPHA, D3DBLEND_INVSRCALPHA, D3DBLENDOP_MAX },
		// BLEND_SCREEN:  final = dest + src*( 1 � dest )
		{ D3DBLEND_INVDESTCOLOR, D3DBLEND_ONE, D3DBLENDOP_ADD },
		// BLEND_LINEARDODGE: final = srca + dest
		{ D3DBLEND_SRCCOLOR, D3DBLEND_SRCCOLOR, D3DBLENDOP_ADD },

		// BLEND_DARKEN: final = min( src, dest )
		{ D3DBLEND_SRCALPHA, D3DBLEND_INVSRCALPHA, D3DBLENDOP_MIN },
		// BLEND_LINEARBURN: final = src + dest - 1
		{ D3DBLEND_SRCCOLOR, D3DBLEND_INVDESTCOLOR, D3DBLENDOP_SUBTRACT },
	};

	assert( blend>=0 && blend<(sizeof(list)/sizeof(list[0])) );

	const rs_t& rs = list[blend];
	dev->SetRenderState( D3DRS_SRCBLEND, rs.src );
	dev->SetRenderState( D3DRS_DESTBLEND, rs.dest );
	dev->SetRenderState( D3DRS_BLENDOP, rs.op );
}
//void CCache::Stats( perf_t::cache_t& out )
//{
//	out = stats;
//
//	// Clear the stats
//	stats.batches = 0;
//	stats.verts = 0;
//	stats.indices = 0;
//	stats.flushes = 0;
//}
bool CCache::Begin( const context_t& ctx, btype_t type, unsigned int verts, unsigned int prims, idtype_t tech, texture_t* tex )
{
#ifdef SHADE_DEBUG
	// If this fails you forgot to call End()... somewhere :p
	if ( !( iVertEnd==0 && iIndexEnd==0 ) )
		__debugbreak();
#endif // SHADE_DEBUG

	colormod = ctx.colormod;

	// Check if buffers are full
	unsigned int idcs = 2*prims;
	if ( type!=BTYPE_LINES ) idcs += prims;

	if ( (iVert+verts)>=MAXVERTS || (iIndex+idcs)>=MAXINDICES )
		Render();

	// Start working with a new batch
	Batch* b = &batches[iBatch];
	if ( !b->type )
		goto reset;

	// Should we start a new batch
	if ( b->type!=type || b->tex!=tex || b->tech!=tech )
	{
		iBatch++;
		if ( iBatch>=MAXBATCHES )
			Render();

		b = &batches[iBatch];
reset:
		b->type = type;
		b->numvert = 0;
		b->numprim = 0;
		b->numidx = 0;
		b->tex = tex;
		b->tech = tech;
		b->ctx = ctx;
	}

	b->numvert += static_cast<unsigned short>(verts);
	b->numprim += static_cast<unsigned short>(prims);
	b->numidx += static_cast<unsigned short>(idcs);

#ifdef SHADE_DEBUG
	// For debugging
	iVertEnd = iVert + verts;
	iIndexEnd = iIndex + idcs;
#endif // SHADE_DEBUG

	return true;
}
void CCache::End()
{
#ifdef SHADE_DEBUG
	// VERY IMPORTANT CHECK if you kept your promise in Begin()
	// If this test fails, the buffers will desync and the whole cache will fail

	if ( !( iVert==iVertEnd && iIndex==iIndexEnd ) )
		__debugbreak();

	iVertEnd = 0;
	iIndexEnd = 0;

	//Render();
#endif // SHADE_DEBUG
}
void CCache::NewBatch()
{
	if ( batches[iBatch].type )
	{
		iBatch++;
		if ( iBatch>=MAXBATCHES ) Render();
		else batches[iBatch].type = BTYPE_EMPTY;
	}
}
void CCache::AddIndicesQuad()
{
	unsigned int v = iVert;
	unsigned int i = iIndex;
	indices[i+0] = v+0;
	indices[i+3] = v+0;
	indices[i+1] = v+1;
	indices[i+2] = v+2;
	indices[i+4] = v+2;
	indices[i+5] = v+3;
	iIndex += 6;

	//AddIndex( 0 );
	//AddIndex( 1 );
	//AddIndex( 2 );
	//AddIndex( 0 );
	//AddIndex( 2 );
	//AddIndex( 3 );
}

#undef THIS
#undef dev

}
}
