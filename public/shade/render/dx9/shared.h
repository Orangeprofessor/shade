#ifndef HGUARD_SHADE_RENDER_DX9_SHARED
#define HGUARD_SHADE_RENDER_DX9_SHARED
#pragma once

#include "../shared.h"
#include "../../time.h"

// Forward declarations for d3d

struct IDirect3D9;
struct IDirect3DDevice9;
struct IDirect3DVertexBuffer9;
struct IDirect3DIndexBuffer9;
struct ID3DXLine;
struct ID3DXSprite;
struct ID3DXFont;
struct IDirect3DBaseTexture9;
struct IDirect3DTexture9;
struct IDirect3DStateBlock9;
struct IDirect3DSurface9;

struct IDirect3DVertexDeclaration9;
struct IDirect3DVertexShader9;
struct IDirect3DPixelShader9;
struct ID3DXConstantTable;
struct ID3DXBuffer;
struct ID3DXEffect;
typedef const char* D3DXHANDLE;

#define SHADEDX9CHECK( EXPR ) \
	do { HRESULT hr = EXPR; assert( SUCCEEDED(hr) ); } while (false)

namespace shade
{
namespace dx9
{

// Vertex for simple 2D drawing
struct Vertex
{
	float x, y;
	unsigned long color;
	float u, v;
};

// DirectX HRESULT errors
class DxError : public Error
{
public:
	DxError( HRESULT hr ) : hr(hr) { }
	virtual void format( string_t& str ) const;
	HRESULT hr;
};
// DirectX Compile errors
class DxCompileError : public DxError
{
public:
	DxCompileError( HRESULT hr, ID3DXBuffer* err, const char_t* file );
	virtual void format( string_t& str ) const;
	string_t desc;
	string_t filename;
};

// Some techniques
enum
{
	TECH_DIFFUSE = 0xf83892ab, //IDTYPE("Diffuse")
	TECH_TEXTURED = 0x35a28550, //IDTYPE("TexturedSimple")
};

// perf_t stats for the dx9 renderer

struct perf_t
{
	Timer timer;
	float fps;
	unsigned int frametime;
	struct cache_t
	{
		unsigned int flushes;
		unsigned int batches;
		unsigned int verts;
		unsigned int indices;
	} stats;

	void begin();
};

}
}

#endif // !HGUARD_SHADE_RENDER_DX9_SHARED
