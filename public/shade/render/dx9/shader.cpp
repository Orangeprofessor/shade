
#include "shader.h"
#include "sdk.h"
#include "dx9.h"

namespace shade
{
namespace dx9
{
#define THIS GETOUTER(CRender,this,shader)
#define dev (THIS->dev)

CShader::CShader() : doclip(false), decl(nullptr), efx(nullptr)
{
	// Create the vertex declaration for use with the shaders.
	static const D3DVERTEXELEMENT9 vformat[] =
	{
		{ 0, 0,  D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		{ 0, 8,  D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,    0 },
		{ 0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		D3DDECL_END()
	};
	HRESULT hr = dev->CreateVertexDeclaration( vformat, &decl );
	if ( FAILED(hr) )
	{
		throw DxError( hr );
	}
	Compile( TEXT("shade.fx") );
}
CShader::~CShader()
{
	efx->Release();
}
void CShader::Compile( const char_t* file )
{
	// Load D3DCompiler_43.dll
	// UNDONE! Used to be necessary when I had multiple raw shaders (and everyone would re-load this dll)
	//HMODULE hm = ::LoadLibraryA( "D3DCompiler_43.dll" );

	// Do stuff to create the effect
	// Will throw on error
	LPD3DXBUFFER errors;
	HRESULT hr = D3DXCreateEffectFromFile( dev, file, NULL, NULL, 0, NULL, &efx, &errors );
	
	// Clean up
	//::FreeLibrary( hm );

	// Throw error on failure
	if ( FAILED(hr) )
	{
		throw DxCompileError( hr, errors, file );
	}

	assert( !errors );

#ifdef SHADE_DEBUG
	Dump( "shade.html" );
#endif // SHADE_DEBUG
}
void CShader::Dump( const char* file )
{
#ifdef SHADE_DEBUG
	LPD3DXBUFFER data;
	HRESULT hr = ::D3DXDisassembleEffect( efx, TRUE, &data );
	assert( SUCCEEDED(hr) && data );
	if ( FILE* h = ::fopen( file, STRDEF("w") ) )
	{
		::fwrite( data->GetBufferPointer(), 1, data->GetBufferSize(), h );
		::fclose( h );
	}
#endif // SHADE_DEBUG
}

void CShader::Setup( const CCache::Batch* b )
{
	HRESULT hr;

	const context_t& ctx = b->ctx;

	// Select a technique
	hr = efx->SetTechnique( Tech( b->tech ) );
	assert( SUCCEEDED(hr) );

	// Set some params
	float screen[] = { static_cast<float>(ctx.clip.width), static_cast<float>(ctx.clip.height) };
	hr = efx->SetFloatArray( STRDEF("screen"), screen, 2 );

	float project[8] = {
		1, 0, static_cast<float>(ctx.dx), 0,
		0, 1, static_cast<float>(ctx.dy), 0,
	};
	hr = efx->SetFloatArray( STRDEF("project"), project, 8 );

	// Set the render surface

	// Adjust scissors rect
	if ( doclip )
	{
		RECT rc = { ctx.clip.left, ctx.clip.top, ctx.clip.left+ctx.clip.width, ctx.clip.top+ctx.clip.height };
		hr = dev->SetScissorRect( &rc );
		assert( SUCCEEDED(hr) );
	}

	// Adjust texture

}
unsigned int CShader::Pass( unsigned int pass )
{
	HRESULT hr;

	if ( !pass )
	{
		// Begin the effect
		hr = efx->Begin( &passes, 0 );
		assert( SUCCEEDED(hr) );
	}
	else
	{
		// End previous pass
		hr = efx->EndPass();
		assert( SUCCEEDED(hr) );
	}
	
	if ( pass<passes )
	{
		// Begin the next pass
		hr = efx->BeginPass( pass );
		assert( SUCCEEDED(hr) );
		return passes;
	}
	return 0;
}
void CShader::Finish()
{
	// End the effect
	HRESULT hr = efx->End();
	assert( SUCCEEDED(hr) );
}
void CShader::Begin( fltime_t time )
{
	HRESULT hr;

	hr = dev->SetVertexDeclaration( decl );
	assert( SUCCEEDED(hr) );
	// FIXME! Who is in charge of scissors?!
	hr = dev->SetRenderState( D3DRS_SCISSORTESTENABLE, true );
	assert( SUCCEEDED(hr) );

	// Time is set once during the rendering phase
	hr = efx->SetFloat( STRDEF("time"), static_cast<float>(time) );
	assert( SUCCEEDED(hr) );
}
void CShader::End()
{
}



void CShader::Lost()
{
	if ( efx )
		efx->OnLostDevice();
}
void CShader::Reset()
{
	if ( efx )
		efx->OnResetDevice();
}
D3DXHANDLE CShader::Tech( idtype_t id )
{
	HRESULT hr;
	D3DXHANDLE it = NULL;
	if ( id )
	{
		while ( SUCCEEDED( hr = efx->FindNextValidTechnique( it, &it ) ) && it )
		{
			D3DXTECHNIQUE_DESC desc;
			hr = efx->GetTechniqueDesc( it, &desc );
			assert( SUCCEEDED(hr) );
			if ( hash::hash( desc.Name )==id )
				return it;
		}
	}

	// There must always exist at least 1 valid technique!
	hr = efx->FindNextValidTechnique( NULL, &it );
	assert( SUCCEEDED(hr) && it );
	return it;
}

#undef dev
#undef THIS

}
}
