
#include "dx9.h"

namespace shade
{

SHADEAPI IRender2D* SHADECC CreateRenderDX9( HWND hWnd, IDirect3D9* d3d, IDirect3DDevice9* dev )
{
	IRender2D* cg = nullptr;
	if ( dx9::InitImports() )
		cg = new dx9::CRender( hWnd, d3d, dev );
	return cg;
}

namespace dx9
{
	
CRender::CRender( HWND hWnd, IDirect3D9* d3d, IDirect3DDevice9* dev ) : mhWnd(hWnd), d3d(d3d), dev(dev)
{
	d3d->AddRef();
	dev->AddRef();

	// Check if we're given a DirectX9Ex interface.
	void* temp;
	mbIsEx = SUCCEEDED( dev->QueryInterface(IID_IDirect3DDevice9Ex,&temp) ) && temp;

	// Create directx resources.
	HRESULT hr = ::D3DXCreateLine( dev, &mpLine );
	assert( SUCCEEDED(hr) );
}
CRender::~CRender()
{
	// Will I need explicit Release() so these get ran in the correct order!?
	mpLine->Release();
	dev->Release();
	d3d->Release();
}


// Management
void CRender::Begin()
{
	perf.begin();

	// Capture device state so it can be restored later.
	// We use ID3DXLine::Begin() to fix some bugs that I don't know how to fix.
	//mpLine->Begin();

	// Set device render states.
	struct rstate_t { D3DRENDERSTATETYPE State; DWORD Value; };
	static const rstate_t list[] =
	{
		{ D3DRS_ZENABLE, FALSE },
		{ D3DRS_CULLMODE, D3DCULL_NONE },
		{ D3DRS_LASTPIXEL, TRUE },
		{ D3DRS_FILLMODE, D3DFILL_SOLID },

		{ D3DRS_LIGHTING, FALSE },
		{ D3DRS_FOGENABLE, FALSE },
		{ D3DRS_SPECULARENABLE, FALSE },
		//{ D3DRS_VERTEXBLEND, D3DVBF_0WEIGHTS },

		{ D3DRS_ALPHATESTENABLE, TRUE },
		{ D3DRS_ALPHABLENDENABLE, TRUE },
		{ D3DRS_ALPHAFUNC, D3DCMP_GREATER },
		{ D3DRS_ALPHAREF, 4 },

		{ D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_ALPHA|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_RED },

		{ D3DRS_ANTIALIASEDLINEENABLE, TRUE },
	};
	typedef const rstate_t* iterator;
	for ( iterator it = list, end = it + sizeof(list)/sizeof(list[0]); it!=end; ++it )
	{
		dev->SetRenderState( it->State, it->Value );
	}

	dev->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_SELECTARG1 );
	dev->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	dev->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );

	dev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );

	// Setup default context.
	memset( &currentContext, 0, sizeof(currentContext) );
	RECT client;
	::GetClientRect( mhWnd, &client );
	currentContext.clip.width = client.right-client.left;
	currentContext.clip.height = client.bottom-client.top;
	currentContext.colormod = color_t(255,255,255,255);

	// Setup shader
	shader.Begin( FloatTime() );
}
void CRender::End()
{
	// Flush cache
	cache.Render();

	// Reverse order of Begin()
	shader.End();
	//mpLine->End();
}
void CRender::OnLostDevice()
{
	mpLine->OnLostDevice();
	cache.Release();
	shader.Lost();
}
void CRender::OnResetDevice()
{
	mpLine->OnResetDevice();
	cache.Init();
	shader.Reset();
}

// Contexts
void CRender::ContextPush( const context_t& ctx, context_t& old )
{
	// Must always start a new batch

}
void CRender::ContextPop( const context_t& old )
{
	// Must always start a new batch

	// Just overwrite the context.
	currentContext = old;
}
void CRender::ContextGet( context_t& ctx )
{
	// Note that we return an explicit copy of the context.
	// Not efficient but it prevents ugly hacks to modify the context directly. Use the damn interface!
	ctx = currentContext;
}


}
}
