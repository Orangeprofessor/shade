
#include "native.h"

namespace shade
{

LRESULT WINAPI SharedWndProc( HWND hwnd, UINT id, WPARAM w, LPARAM l );

CNative::CNative( HINSTANCE hInst, const char_t* lpszWindowName ) : mhInst(hInst), gui(nullptr), minft(0)
{
	//
	// Step 1: Registering the Window Class
	//

	static const char_t szClassName[] = TEXT("CNative");
	static WNDCLASSEX wc =
	{
		0, 0, &SharedWndProc,
		0, 0, NULL,
		NULL, NULL, NULL/*(HBRUSH)(COLOR_WINDOW+1)*/,
		NULL, szClassName, NULL
	};

	if ( !wc.cbSize )
	{
		wc.cbSize        = sizeof(WNDCLASSEX);
		//wc.style         = 0;
		//wc.lpfnWndProc   = &SharedWndProc;
		//wc.cbClsExtra    = 0;
		//wc.cbWndExtra    = 0;
		wc.hInstance     = mhInst;
		wc.hIcon         = ::LoadIcon( NULL, IDI_APPLICATION );
		wc.hIconSm		 = wc.hIcon;
		wc.hCursor       = ::LoadCursor( NULL, IDC_ARROW );
		//wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
		//wc.lpszMenuName  = NULL;
		//wc.lpszClassName = szClassName;

		if ( ! ::RegisterClassEx( &wc ) )
		{
			throw std::exception( "CNative::CNative() Failed to Register Window!" );
		}
	}

	//
	// Step 2: Creating the Window
	//

	mhWnd = ::CreateWindowEx( 0,
		szClassName,
		lpszWindowName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 640, 480,
		NULL, NULL, mhInst, this );

	if ( !mhWnd )
	{
		throw std::exception( "CNative::CNative() Failed to Create Window!" );
	}

	timer.Sync();
}
CNative::~CNative()
{
	if ( mhWnd )
		::DestroyWindow( mhWnd );
}
int CNative::MessageLoop()
{
	//
	// Step 3: The Message Loop with APC support
	// weblogs.asp.net/kennykerr/archive/2007/12/12/parallel-programming-with-c-part-2-asynchronous-procedure-calls-and-window-messages.aspx
	//

	MSG msg;
	BOOL b;

	while ( true )
	{
		DWORD result = ::MsgWaitForMultipleObjectsEx( 0, 0, INFINITE, QS_ALLINPUT, MWMO_ALERTABLE|MWMO_INPUTAVAILABLE );
		switch ( result )
		{
		case WAIT_FAILED:
			// Some error happened
			assert( false );
			return ::GetLastError();

		case WAIT_IO_COMPLETION:
			// APCs are handled
			break;

		case WAIT_OBJECT_0:
			// Handle all pending messages
			do
			{
				if ( b = ::PeekMessage( &msg, 0, 0, 0, PM_REMOVE ) )
				{
					if ( msg.message==WM_QUIT )
						return msg.wParam;

					::TranslateMessage( &msg );
					::DispatchMessage( &msg );
				}

				// Need to drop out of this loop after a single WM_PAINT msg.
				// DirectX will ensure there's always a WM_PAINT so we'd never drop out otherwise.
			}
			while ( b && msg.message!=WM_PAINT );
			break;

		default:
			// Should never reach here!
			assert( false );
			break;
		}
	}
}
bool CNative::WndProc( MSG& msg )
{
	//if ( gui && gui->WndProc( msg ) )
	//{
	//	return true;
	//}
	switch ( msg.message )
	{
	case WM_PAINT:
		Paint();
		//InvalidateRect( mhWnd, NULL, FALSE );
		return true;
	case WM_CLOSE:
		// Don't handle (by returning true) this message to give others a chance to see it.
		PostQuitMessage( 0 );
	default:
		return false;
	}
}
void CNative::Paint()
{
}
bool CNative::Show( bool b )
{
	return
		::ShowWindow( mhWnd, b )!=FALSE &&
		::UpdateWindow( mhWnd )!=FALSE;
}
void CNative::FrameWait()
{
	fltime_t time = timer.Delta();
	if ( time<minft )
	{
		// Wait until at least minft seconds have passed

		// SleepEx because we want APC support
		// 1ms to fix a bug where we're not actually sleeping
		do SleepEx( 1, TRUE );
		while ( timer.Delta()<minft );
		// Non-APC version
		//Sleep( static_cast<int>((minft-time)*1000.0f)-1 );

		// At this point we possibly waited a little bit too long due to Sleep inaccuracies
		// So we can't just timer.Sync(), instead add 'minft' to the timer
		timer.BaseAdd( minft );
	}
	else
	{
		// Note the time we start rendering
		timer.Sync();
	}
}
LRESULT WINAPI SharedWndProc( HWND hwnd, UINT id, WPARAM w, LPARAM l )
{
	// On creation store the this pointer in the hwnd
	// Note: WM_GETMINMAXINFO is called before this
	if ( id==WM_NCCREATE )
	{
		LPCREATESTRUCT cs = (LPCREATESTRUCT) l;
		::SetWindowLongPtr( hwnd, GWL_USERDATA, (LONG_PTR)cs->lpCreateParams );
	}

	// Invoke our WndProc
	if ( CNative* ptr = reinterpret_cast<CNative*>( ::GetWindowLongPtr( hwnd, GWL_USERDATA ) ) )
	{
		MSG msg = { hwnd, id, w, l };
		if ( ptr->WndProc( msg ) )
			return S_OK;
	}

	// Handle any unhandled messages
	return ::DefWindowProc( hwnd, id, w, l );
}

}
