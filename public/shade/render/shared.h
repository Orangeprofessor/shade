#ifndef HGUARD_SHADE_RENDER_SHARED
#define HGUARD_SHADE_RENDER_SHARED
#pragma once

#include "../base.h"

namespace shade
{

// Coordinate units
typedef SHADE_UNIT unit;

// Basic structures

struct point_t;
struct rect_t;
struct area_t;

struct point_t
{
	point_t() {}
	point_t( const point_t& p ) : x(p.x), y(p.y) {}
	point_t( unit x, unit y ) : x(x), y(y) {}

	bool within( const rect_t& rc ) const;

	unit x;
	unit y;
};

struct rect_t
{
	rect_t() {}
	rect_t( const rect_t& rc ) : left(rc.left), top(rc.top), width(rc.width), height(rc.height) {}
	rect_t( unit left, unit top, unit width, unit height ) : left(left), top(top), width(width), height(height) {}

	unit left;
	unit top;
	unit width;
	unit height;
};

struct area_t
{
	area_t() {}
	area_t( const area_t& rc ) : left(rc.left), top(rc.top), right(rc.right), bottom(rc.bottom) {}
	area_t( unit left, unit top, unit right, unit bottom ) : left(left), top(top), right(right), bottom(bottom) {}
	explicit area_t( unit x ) : left(x), top(x), right(x), bottom(x) {}

	unit left;
	unit top;
	unit right;
	unit bottom;
};


	
inline bool point_t::within( const rect_t& rc ) const
{
	unit x = this->x - rc.left;
	if ( x<0 || x>=rc.width ) return false;
	unit y = this->y - rc.top;
	if ( y<0 || y>=rc.height ) return false;
	return true;
}


//
// Font resources
//
// Some flags can be added later by using the 'flags' parameter of SetFont().
// (somewhat dependent on the renderer, will use native support if available.)
//

enum fntflag_t
{
	FONT_CLEAR = 0,
	FONT_ITALIC = (1<<0),
	FONT_UNDERLINE = (1<<1),
	FONT_STRIKEOUT = (1<<2),
	FONT_HIGHLIGHT = (1<<3),
	FONT_OUTLINE = (1<<4),
	FONT_DROPSHADOW = (1<<5),
	FONT_ANTIALIAS = (1<<6),
	FONT_QUALITY = (1<<7),
};
ENUMLOGIC( fntflag_t );

struct logfont_t
{
	logfont_t() { }
	logfont_t( const char_t* faceName, unsigned short tall, unsigned short weight = 0, fntflag_t flags = FONT_CLEAR )
		: tall(tall), weight(weight), flags(flags), spacing(0)
	{
		char_traits::copy( this->faceName, faceName, sizeof(this->faceName) );
	}
	char_t faceName[32];
	unsigned short tall;
	unsigned short weight;
	fntflag_t flags;
	unsigned char spacing;
	unsigned char _padme[3];
};

typedef const logfont_t* hfont;




//
// Sprite resources
//
// Create using the image_t struct and/or LoadImage functions.
// Shouldn't need to initialize these directly.
//
// Special usage: sometimes sprites are grouped together in an atlas
// Instead of saving each individual part of the atlas you need only specifiy the first sprite of a series
// Then call ->at( index ) with the desired offset to retrieve the sibling sprites
// May return an invalid handle or NULL for out of range values (implementation defined).
//

struct logsprite_t
{
	const logsprite_t* (SHADECC* _at)( const logsprite_t* ptr, unsigned int idx );
	inline const logsprite_t* at( unsigned int idx ) const { return _at( this, idx ); }

	unsigned int width;
	unsigned int height;
};
typedef const logsprite_t* hsprite;




//
// Render targets
//
// Targets are buffers to draw on.
//

struct logtarget_t
{
	// For a screen buffer leave both 0
	logtarget_t() : width(0), height(0) {}
	// Buffer of a specific size
	logtarget_t( int width, int height ) : width(width), height(height) {}

	int width;
	int height;
};
typedef const logtarget_t* htarget;



//
// Blending modes
//
// Most of these don't play nice with alpha channels.
// WARNING! NUMBERS ARE INDEXED IN AN ARRAY, DO NOT MODIFY!
// FIXME! Figure out what to do with unsupported blend modes?
//

enum blend_t
{
	BLEND_SOLID = 0,
	BLEND_ALPHA,
	//BLEND_SUBPIXEL, // For subpixel text rendering... Figure me out?
		
	BLEND_LIGHTEN,
	BLEND_SCREEN,
	//BLEND_COLORDODGE,
	BLEND_LINEARDODGE,

	BLEND_DARKEN,
	//BLEND_MULTIPLY,
	//BLEND_COLORBURN,
	BLEND_LINEARBURN,
};

//
// Drawing context
//
// clip   : Clip region
// dx, dy : Number of pixels shifted to the top-left
// alpha  : Apply to all operations
// blend  : Blend operation
// target : Target surface to render on
//

struct context_t
{
	context_t() { }
	context_t( int width, int height ) : clip(rect_t(0,0,width,height)), dx(0), dy(0), colormod(~0), blend(BLEND_ALPHA), target(0) { }
	context_t( const rect_t& clip, unsigned int alpha, int dx = 0, int dy = 0 ) : clip(clip), dx(dx), dy(dy), colormod(255,255,255,alpha), blend(BLEND_ALPHA), target(0) { }

	rect_t clip;
	int dx, dy;
	color_t colormod;
	blend_t blend;
	htarget target;

	inline void Merge( const context_t& ctx )
	{
		unit right = clip.left + clip.width;
		clip.left += ctx.clip.left;
		clip.width = ctx.clip.width;
		if ( (clip.left+clip.width)>right )
			clip.width = right - clip.left;

		unit bottom = clip.top + clip.height;
		clip.top += ctx.clip.top;
		clip.height = ctx.clip.height;
		if ( (clip.top+clip.height)>bottom )
			clip.height = bottom - clip.top;

		dx += ctx.dx;
		dy += ctx.dy;

		//colormod.red( ctx.colormod.red() );
		//colormod.green( ctx.colormod.green() );
		//colormod.blue( ctx.colormod.blue() );
		colormod.alpha( (colormod.alpha()*ctx.colormod.alpha())/255u );
	}
	inline int AdjustX( int x ) const
	{
		return x + clip.left - dx;
	}
	inline int AdjustY( int y ) const
	{
		return y + clip.top - dy;
	}
	inline color_t AdjustC( color_t c ) const
	{
		// Fast alpha merge
		unsigned int a = (c.alpha()*colormod.alpha())>>8; if ( a ) a++;
		c.alpha( a );
		return c;
	}
	inline void Adjust( int& x, int& y ) const
	{
		x = AdjustX( x );
		y = AdjustY( y );
	}
};

}

#endif // !HGUARD_SHADE_RENDER_SHARED
