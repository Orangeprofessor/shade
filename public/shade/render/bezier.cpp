
#include "bezier.h"

namespace shade
{


SHADEAPI void SHADECC PtCubicBezier( const point_t pts[4], float flt, float* xy )
{
	__m128 apts[2];
#if defined(SHADE_UNIT_INT)
	// Load p1 and p2 in xmm register
	// x1  y1  x2  y2
	apts[0] = _mm_cvtepi32_ps( _mm_loadu_si128( (const __m128i*)&pts[0] ) );
	// Load p3 and p4 in xmm register
	// x3  y3  x4  y4
	apts[1] = _mm_cvtepi32_ps( _mm_loadu_si128( (const __m128i*)&pts[2] ) );
#elif defined(SHADE_UNIT_FLOAT)
	apts[0] = _mm_loadu_ps( &pts[0].x );
	apts[1] = _mm_loadu_ps( &pts[2].x );
#else
#error
#endif
	SSE_CubicBezier( apts, flt, xy );
}
// Fast SSE version (will probably be inlined!)
// Performance; Func: 0.0051, Inlined: 0.0039, Ref: 0.0079
// So SSE is about twice as fast...
void SSE_CubicBezier( const __m128 apts[2], float flt, float* xy )
{
	static const float one = 1.0f;
	static const float three = 3.0f;

	// t = 0 --to-> 1
	__m128 t = _mm_load_ps1( &flt );
	// s = 1 --to-> 0
	__m128 s = _mm_sub_ps( _mm_load_ps1(&one), t );
	// 3  3  3  3
	__m128 c = _mm_load_ps1( &three );

	// s  s  3*t  3*t
	__m128 r1 = _mm_shuffle_ps( s, _mm_mul_ps( c, t ), _MM_SHUFFLE(3,2,1,0) );
	// s*s*s  s*s*s  3*t*s*s  3*t*s*s
	       r1 = _mm_mul_ps( r1, _mm_mul_ps( s, s ) );
	// apply factors to coordinates
	       r1 = _mm_mul_ps( r1, apts[0] );

	// 3*s  3*s  t  t
	__m128 r2 = _mm_shuffle_ps( _mm_mul_ps( c, s ), t, _MM_SHUFFLE(3,2,1,0) );
	// 3*s*t*t  3*s*t*t  t*t*t  t*t*t
	       r2 = _mm_mul_ps( r2, _mm_mul_ps( t, t ) );
	// apply factors to the coordinates
	       r2 = _mm_mul_ps( r2, apts[1] );

	// x1+x3  y1+y3  x2+x4  y2+y4
	__m128 f = _mm_add_ps( r1, r2 );
	// x1+x2+x3+x4  y1+y2+y3+y4
	       f = _mm_add_ps( _mm_shuffle_ps(f,f,_MM_SHUFFLE(1,0,3,2)), f );

	_mm_storel_epi64( (__m128i*)xy, _mm_castps_si128(f) );
}
// Reference implementation
void PtCubicBezierRef( const point_t pts[4], float t, float* xy )
{
	float s = 1.0f - t;

	xy[0] = s*s*s*static_cast<float>(pts[0].x) +
		3*s*s*t*static_cast<float>(pts[1].x) +
		3*s*t*t*static_cast<float>(pts[2].x) +
		t*t*t*static_cast<float>(pts[3].x);
	xy[1] = s*s*s*static_cast<float>(pts[0].y) +
		3*s*s*t*static_cast<float>(pts[1].y) +
		3*s*t*t*static_cast<float>(pts[2].y) +
		t*t*t*static_cast<float>(pts[3].y);
}

}
