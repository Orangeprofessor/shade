#ifndef HGUARD_SHADE_LOCALIZE
#define HGUARD_SHADE_LOCALIZE
#pragma once

//----------------------------------------------------------------
// Localization support
//----------------------------------------------------------------

#include "base.h"

namespace shade
{

class ILocalize;


// Create a localization instance for specified language codename.
SHADEAPI ILocalize* SHADECC CreateLocalization( const char* lang );

// Decode UTF8 sequence, where do I belong?
SHADEAPI unsigned int SHADECC DecodeUtf8( const char*& ptr, unsigned int res = '?' );


INTERFACE ILocalize
{
public:
	virtual ~ILocalize() {}

	// Gets the codename for the language, get the full name for display lookup #LangName.
	// Should be passed during construction and be un-changeable.
	virtual const char* Language() const = 0;


	//------------------------------------------------
	// Loading
	//------------------------------------------------
	// Reads data from a KvNode data structure.
	// Not sure what to do with the language attribute, ex:
	// lang("en_US") { #LangName: "English (US)"; }
	virtual void Parse( KvNode* kv );
	// Add a translation
	virtual bool AddString( const char_t* id, const char_t* value ) = 0;
	// Clears all translations
	virtual void RemoveAll() = 0;


	//------------------------------------------------
	// Indices
	//------------------------------------------------
	// Returns an (explicit) index that can be stored & used later.
	// No guaranties that this index is actually valid (atm you cannot verify this, fixme?).
	// Format of the identifier usually follows #TranslatedString.
	// The pound sign isn't required but other systems will only localize if it is present (and use the string unlocalized if not).
	//
	// This whole Index thing is a wrapper to make direct string literals play nice with my grand scheme of hashing.

	typedef unsigned int index_t;

protected:
	struct Index
	{
		template< unsigned int S >
		INLINE Index( const char_t (&name)[S] ) : id(HASH(name)) { }
		//inline Index( const char* name ) : id(hash::hash(name)) { }
		//inline Index( const wchar_t* name ) : id(hash::hash(name)) { }
		inline Index( index_t id ) : id(id) { }
		inline operator index_t () { return id; }
		index_t id;
	};
public:

	inline Index FindIndex( Index id ) const
	{
		return id;
	}
	// Ack! This will override the template version...
	// FIXME! How else can I handle this?
	//inline index_t FindIndex( const char* id ) const
	//{
	//	return hash::hash(id);
	//}


	//------------------------------------------------
	// Lookup strings
	//------------------------------------------------
	// Direct lookups not supported at the moment because it doesn't play nice with encryption.



	//------------------------------------------------
	// Formatting strings
	//------------------------------------------------
	// [size] is a buffer size, [len] are a number of characters.

	// Format a localized string in UTF-8
			int Format( Index id, char* buf, unsigned size, ... ) const;
	virtual int FormatEx( Index id, char* buf, unsigned size, va_list va = nullptr ) const = 0;
	// Format a localized string in UTF-16
			int Format( Index id, wchar_t* buf, unsigned size, ... ) const;
	virtual int FormatEx( Index id, wchar_t* buf, unsigned size, va_list va = nullptr ) const = 0;


	// Convenience stuff

	template< typename T, unsigned L >
	class Buffer
	{
	public:
		Buffer() {}
		Buffer( ILocalize* loc, Index id, ... )
		{
			va_list va;
			va_start( id, va );
			loc->FormatEx( id, _temp, sizeof(_temp), va );
			va_end( va );
		}
		inline operator T* () { return _temp; }
		inline operator const T* () const { return _temp; }
	private:
		T _temp[L];
	};



	//------------------------------------------------
	// Locale stuff
	//------------------------------------------------
	// TODO: Add in functionality by the traditional C++ locale?
	// Not sure how far I want to go with this...

	// Convert UTF-16 to UTF-8
	// Returns number of outputted characters
	virtual int ConvertToUtf8( char* out, int size, const wchar_t* in, int len = -1 ) const;
	// Convert UTF-8 to UTF-16
	// Returns number of outputted characters
	virtual int ConvertToUtf16( wchar_t* out, int size, const char* in, int len = -1 ) const;
	// Get unicode information wrapper
	// Seems like something that would belong here.
	//virtual bool GetUnicodeDesc( unsigned int c, unicodesc_t& desc ) const;
};




inline int ILocalize::Format( Index id, char* buf, unsigned size, ... ) const
{
	va_list va;
	va_start( va, size );
	return FormatEx( id, buf, size, va );
}
inline int ILocalize::Format( Index id, wchar_t* buf, unsigned size, ... ) const
{
	va_list va;
	va_start( va, size );
	return FormatEx( id, buf, size, va );
}

}

#endif // !HGUARD_SHADE_LOCALIZE
